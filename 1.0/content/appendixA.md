## Exempel i CSV, kommaseparerad

Nedan är en rad i en fil som representerar en fiktiv grillplats i Gullspångs kommun.

Flera attribut i exemplet är tomma och har därför flera kommatecken efter varandra. Det betyder att egenskapen för grillplatsen är okänd. Om du inte känner till en egenskap på grillplatsen skall attributet vara tomt. "False" används enbart när du vet att egenskapen inte finns. I exemplet är attributen “water” och “drinking_water” tomma, dvs i exemplet känner man inte till om det finns vatten eller dricksvatten på platsen. 

Om du har flera fält som du lämnar tomma, kanske det är dags att kommunen inventerar grillplatsen på nytt för att kunna förse datakonsumenten med så aktuell data som möjligt. 

Observera dubbelt citationstecken “ kring fritextfält som innehåller ett kommatecken nedan.

<div class="example csvtext">
source,id,name,latitude,longitude,updated,wikidata,visit_url,email,description,postcode,city,country,restrictions,wood_storage,grate,water,drinking_water,shoreline,boat_required,wind_shelter,cabin,campground,toilet,seating,table,trashbin,beach,snowmobile_trail,playground,parking,wheelchair,accessibility,td_url,service_interval

2120001637,2637-gp-asnehojden,Åsnehöjdens Grillplats,58.857540,14.217260,2022-06-07,Q1631543,"https://gullspang.se/Gullspangs-kommun ",info@gullspang.se,"Liten grillplats med eldstad i betong, fin utsikt",54730,Gullspång,SE,"Öppet året runt men svårt att ta sig till platsen under vintertid",false,true,,,false,false,false,false,true,true,true,true,false,false,false,false,true,yes,"Platsen är helt anpassad för åtkomst med rullstol, har slätt hårt underlag och är inte kuperad. Reserverade parkeringsplatser för rörelsehindrad finns.",,QUARTERLY
</div>

## Exempel i CSV, semikolonseparerad
Observera att flera fält är tomma och då helt saknar innehåll, istället är de tänkta värdena ersatta med ett semikolon.

<div class="example csvtext">
source;id;name;latitude;longitude;updated;wikidata;visit_url;email;description;postcode;city;country;restrictions;wood_storage;grate;water;drinking_water;shoreline;boat_required;wind_shelter;cabin;campground;toilet;seating;table;trashbin;beach;snowmobile_trail;playground;parking;wheelchair;accessibility;td_url;service_interval<br>
2120001637;2637-gp-asnehojden;Åsnehöjdens Grillplats;58.857540;14.217260;2022-06-07;Q1631543;"https://gullspang.se/Gullspangs-kommun ";info@gullspang.se;"Liten grillplats med eldstad i betong, fin utsikt";54730;Gullspång;SE;"Öppet året runt men svårt att ta sig till platsen under vintertid";false;true;;;false;false;false;false;true;true;true;true;false;false;false;false;true;yes;"Platsen är helt anpassad för åtkomst med rullstol; har slätt hårt underlag och är inte kuperad. Reserverade parkeringsplatser för rörelsehindrad finns.";;QUARTERLY
</div>