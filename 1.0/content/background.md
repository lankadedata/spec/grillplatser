**Bakgrund**

Under 2022 önskade MTG-kommunerna inventera och upprätta förbättrad register och skötselplan för sina grillplatser i kommunen. 

Under arbetet med specifikationen för en sådan datamängd, kontaktades webbtjänsten [grillplatser.nu](https://grillplatser.nu) med tanke om att skapa en specifikation som kan nyttjas för att dels beskriva grillplatser på denna tjänst, för kommunens egna behov och för allmänhetens intresse.

Specifikationen som blev resultatet lanseras och har tagits fram inom ramen för samverkansprojektet “Dataportal Väst”.
