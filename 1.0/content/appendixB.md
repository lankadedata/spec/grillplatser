# Exempel i JSON

Samma grillplats som i Appendix A, fast nu som JSON.

```json
{
    "source": "2120001637",
    "id":  "2637-gp-asnehojdens",
    "name":  "Åsnehöjdens Grillplats",
    "latitude":  "58.857540",
    "longitude":  "14.217260",
    "updated":  "2022-06-07",
    "wikidata":  "Q1631543",
    "visit_url":  "https://gullspang.se/Gullspangs-kommun ",
    "email":  "info@gullspang.se",
    "description":  "Liten grillplats med eldstad i betong, fin utsikt",
    "postcode":  "54730",
    "city":  "Gullspång",
    "country":  "SE",
    "restrictions":  "Öppet året runt men svårt att ta sig till platsen under vintertid",
    "wood_storage":  "false",
    "grate":  "true",
    "water":  "",
    "drinking_water":  "",
    "shoreline":  "false",
    "boat_required":  "false",
    "wind_shelter":  "false",
    "cabin":  "false",
    "campground":  "true",
    "toilet":  "true",
    "seating":  "true",
    "table":  "true",
    "trashbin":  "false",
    "beach":  "false",
    "snowmobile_trail":  "false",
    "playground":  "true",
    "parking":  "true",
    "wheelchair":  "yes",
    "accessibility":  "Platsen är helt anpassad för åtkomst med rullstol, har slätt hårt underlag och är inte kuperad. Reserverade parkeringsplatser för rörelsehindrad finns.",
    "td_url":  "",
    "service_interval":  "QUARTERLY",
}
```
