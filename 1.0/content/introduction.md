# Introduktion
Med grillplats menas en särskilt anlagd eller skött allmän plats där man kan göra upp eld och/eller grilla i en särskilt anlagd härd.

Denna specifikation definierar en enkel tabulär informationsmodell för grillplatser. Specifikationen innefattar också en beskrivning av hur informationen uttrycks i formatet CSV och JSON. Som grund förutsätts CSV kunna levereras då det är praktiskt format och skapar förutsägbarhet för mottagare av informationen. Det är också ett format som gör sammanställningen av datamängden enklare då den kan representeras i t.ex. ett kalkylblad.