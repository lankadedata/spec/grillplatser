# Datamodell
Datamodellen är tabulär, där varje rad motsvarar exakt en grillplats och varje kolumn motsvarar en egenskap för denna plats. 35 attribut är definierade, där de första 6 är obligatoriska. Speciellt viktigt är att det finns en [identifierare](#id) då det möjliggör att man på ett unikt sätt kan referera till enskilda grillplatser. Identifieraren ska vara unik och beständig över tid och återanvändning av identifieraren avråds från (t.ex. om en grillplats läggs ned). Se [förtydliganden](#fortydligande-av-attribut).

<div class="note" title="1">
Vi väljer att använda beskrivande men korta kolumnnamn som uttrycks med gemener, utan mellanslag (understreck för att separera ord) och på engelska.

Orsaken till detta är att man vill använda kolumnnamn snarare än ordningen för att detektera om viss data finns. Korta och enkla kolumnnamn minskar risken för att problem uppstår. Det är vanligt att man erbjuder söktjänster där frågor mot olika kolumner skrivs in i webbadresser och det är bra om det kan göras utan speciella teckenkodningar.

Genom engelska attributnamn blir modellen enklare att hantera i programvaror och tjänster utvecklade utanför Sverige.
</div>

<div class="note" title="2">
Kolumn 4 och 5, latitude och longitude, anges på “WGS84-format”. Se [Förtydligande attribut](#fortydligande-av-attribut) för förklaring. Detta är det breda vedertagna koordinatsystem som används av många kommersiellt tillgängliga kart- och  navigationsverktyg. De flesta av dessa verktyg gör det enkelt att ange eller plocka ut koordinater för en plats i detta format.

För dataproducenten kan [OpenStreetMap](https://www.openstreetmap.org/) rekommenderas för att t.ex. hitta koordinater för en viss plats genom att placera en kartnål och kopiera kartnålens koordinat.

Den som kan använda koordinater från kommunens GIS-system - tala med din GIS- eller Kart- och mätenhet för att säkerställa att dina koordinater är i WGS84-formatet.
</div>

<div class="ms_datatable">

| #  | Kolumnnamn      | Datatyp                         | Exempel och förklaring |
| -- | :---------------:| :-------------------------------: | ---------------------- |
| 1  | source           | heltal                         | **Obligatoriskt** - Ange organisationsnummret utan mellanslag eller bindesstreck för organisationen. Exempel för Gullspångs kommun: “2120001637”. |
| 2  | id               | text                           | **Obligatoriskt** - Ange en unik och stabil identifierare för grillplatsen inom er kommun. Det viktiga är att identifieraren är unik (tänk i hela världen), och att den är stabil över tid - d.v.s. att den aldrig kommer ändras. Om en grillplats avvecklas ska identifieraren inte återanvändas på en annan grillplats. Läs mer om [id](#id). <br>|
| 3  | name            | text                            | **Obligatoriskt** - Grillplatsens namn, exempel för en grillplats i Mariestads kommun “Gästhamnens Grillplats” eller för grillplats i Stockholms kommun “Åsnehöjdens Grillplats” resp. “Åsnehöjdens norra grillplats”. <br>|
| 4  | latitude        | decimaltal                      | **Obligatoriskt** - Latitud anges per format som [WGS84](#latitud). <br>|
| 5  | longitude       | decimaltal                      | **Obligatoriskt** - Longitud anges per format som [WGS84](#longitud).<br>|
| 6  | updated         | datum                           | **Obligatoriskt** - Ett datum som anger när informationen om grillplatsen senast uppdaterades. Se [förtydligande](#updated). |
| 7  | wikidata        | text                            | Referens till Wikidata-artikel/entry om platsen. Se [Wikidata](#wikidata). Använd den sk. Q-koden till entiteten på Wikidata.|
| 8  | visit_url       | URL                             | Länk till allmän besöksinformation för platsen, t.ex. turistinformation eller liknande.|
| 9  | email           | text                            | E-postadress för vidare kontakt, anges med gemener och med @ som avdelare. Ange ej det inledande URI schemat `mailto:` eller HTML-koder i uttrycket. Exempel: info@molndal.se <br>|
| 10 | description     | text                            | Kort beskrivning av platsen och omgivningen. Undvik att beskriva sådant som redan framgår i specifikationen. |
| 11 | postcode        | text                            | Postnummer, exempelvis “46430”, mappar mot OpenStreetMaps “addr:postcode”. Läs mer hos [OSM](https://wiki.openstreetmap.org/wiki/Key:addr). <br>|
| 12 | city            | text                            | Postort, exempelvis “Mellerud”. Mappar mot OpenStreetMaps “addr:city”. Läs mer hos [OSM](https://wiki.openstreetmap.org/wiki/Key:addr). <br>|
| 13 | country         | text                            | Land där grillplatsen finns. Skall anges enligt [ISO3166-1](https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2). För Sverige ange “SE”. Fältet mappar mot OpenStreetMaps [addr:country](https://wiki.openstreetmap.org/wiki/Key:addr:country). <br>|
| 14 | restrictions    | text                            | Beskriver eventuella restriktioner eller förhållningsregler för platsen. Se [restrictions](#restrictions). |
| 15 | wood_storage    | boolean                         | Anger om ett särskilt anordnat vedförråd finns på platsen, men beskriver inte om förrådet innehåller någon ved just nu. |
| 16 | grate           | boolean                         | Anger om det finns ett fast eller flyttbart grillgaller på platsen för eldstaden. Beskriver ej funktion eller skick. |
| 17 | water           | boolean                         | Anger om det finns tillgång till vatten för att exempelvis släcka eld.  |
| 18 | drinking_water  | boolean                         | Beskriver om det finns dricksvatten på platsen i någon form. Se [förtydligande](#drinking_water). |
| 19 | shoreline       | boolean                         | Anger om det finns strandkant, åbrink, bäckfåra eller liknande i närheten.  |
| 20 | boat_required   | boolean                         | Beskriver om platsen endast är åtkomlig genom båt. |
| 21 | wind_shelter    | boolean                         | Anger om det finns något vindskydd. |
| 22 | cabin           | boolean                         | Anger om det finns en raststuga med väggar och tak. En övernattningsstuga är att jämställa med en raststuga. Se [bild](https://wiki.openstreetmap.org/wiki/File:Koch_Cabin.JPG).|
| 23 | campground      | boolean                         | Anger om det är tillåtet att campa vid grillplatsen. |
| 24 | toilet          | boolean                         | Finns toalett av någon typ. Exempelvis spol- kemtoa, torrdass eller annan typ av toalett. |
| 25 | seating         | boolean                         | Finns anordnade sittplatser i form av bänkar eller stolar. |
| 26 | table           | boolean                         | Finns särskilt anordnat bord eller sittmöbel med bord. |
| 27 | trashbin        | boolean                         | Finns soptunna i någon form på platsen. |
| 28 | beach           | boolean                         | Finns badstrand i direkt anslutning till grillplatsen. |
| 29 | snowmobile_trail| boolean                         | Finns skoterled i direkt anslutning till platsen. |
| 30 | playground      | boolean                         | Finns lekplats i direkt anslutning till grillplatsen. |
| 31 | parking         | boolean                         | Beskriver om det i närheten till grillplatsen (kortare än 100 meter) finns en parkeringsplats till vilken besökare av grillplatsen har tillgång. |
| 32 | wheelchair      | yes/no/limited                  | Beskriver till vilken grad grillplatser är anpassad för rullstolar. Se [förtydligande](#wheelchair). |
| 33 | accessibility   | text                            | Ytterligare beskrivning om hur tillgänglighetsanpassad grillplatsen är. | 
| 34 | td_url          | URL                             | Länk till [Tillgänglighetsdatabasen (TD)](#tillganglighetsdatabasen). |
| 35 | service_interval| UNKNOWN/IRREG/<br>YEARLY/QUARTERLY/<br>MONTHLY/WEEKLY/DAILY | Ange i vilken frekvens grillplatsen underhålls. Se [förtydligande](#service_interval). |

</div>

## Förtydligande av datatyper
En del av datatyperna nedan förtydligas med hjälp av det som kallas reguljära uttryck. Dessa är uttryckta så att de matchar exakt, dvs inga inledande eller eftersläpande tecken tillåts.

### **heltal**
Reguljärt uttryck: **`/^\-?\\d+$/`**

Heltal anges alltid som en radda siffror utan mellanrum eventuellt med ett inledande minus. Se [xsd:integer](https://www.w3.org/TR/xmlschema-2/#integer) för en längre definition. Utelämnat värde tolkas aldrig som noll (0) utan tolkas som “avsaknad av värde”.

### **decimaltal**
Reguljärt uttryck: **`/^\-?\\d+\\.\\d+$/`**

Decimaltal anges i enlighet med [xsd:decimal](https://www.w3.org/TR/xmlschema-2/#decimal). Notera att i Sverige används ofta decimalkomma inte punkt. För att vara enhetlig mellan olika dataformat ska decimalpunkt användas istället (då undviks problem med CSV formatet som använder komma som separator).

Den kanoniska representationen i xsd:decimal är påbjuden, d.v.s. inga inledande nollor eller +, samt att man alltid ska ha en siffra innan och efter decimalpunkt. Noll skrivs som 0.0 och ett utelämnat värde skall aldrig tolkas som noll (0) utan “avsaknad av värde”.

### **url**
En URL är en webbadress enligt specifikationen RFC 1738. I detta sammanhang förutsätts schemana http eller https. För webbadresser med speciella tecken tillåts UTF-8-encodade domäner och pather, se då RFC 3986.

Observera att man inte får utelämna schemat, d.v.s. "www.example.com" är inte en tillåten webbadress, däremot är "http://www.example.com" ok. Relativa webbadresser accepteras inte heller. (En fullständig regular expression utelämnas då den är både för omfattande och opedagogisk.)

Om du behöver ange flera URL:er måste du då sätta dubbelt citattecken och separera de olika URL:erna med kommatecken. Läs gärna med i RFC 4180 för CSV.

Kontrollera även innan du publicerar filen att det går att läsa in din fil utan problem. Det finns flera webbverktyg för att testa så kallad [parsing](https://sv.wikipedia.org/wiki/Parser) t.ex. [https://CSVLint.io](https://csvlint.io) som du kan använda för att testa din fil.

### **boolean**
Reguljärt uttryck: **`/^[true|false]?&/`**

I samtliga förekommande fall kan texten “true” eller “false” utelämnas.

Ett tomt fält skall tolkas som “okänt” eller “ej inventerat”. Ange enbart “true” eller “false” om du vet att egenskapen finns (true) eller saknas (false). Gissa aldrig.

Attributet är en så kallad “Boolesk” datatyp och kan antingen ha ett av två värden: Sant eller Falskt, men aldrig båda.

### **phone**
Reguljärt uttryck: **`/^\+?[\d-]{6,12}$/`**

För bästa internationella funktion rekommenderas att ange telefonnummer i internationellt format enligt [RFC3966](https://www.ietf.org/rfc/rfc3966). Då anger du landskod med + före riktnummer, utan inledande nolla på riktnummer. Telefonnummer till Borås Stad växel, 033-357000 anges internationell på följande sätt: `+46-33-357000`

Andra godtagbara och möjliga format, istället för internationellt nummer: `033-357000`

**Observera** att man inte får inkludera mellanslag i telefonummret då man vill kunna generera en webbadress (URI) utifrån numret vilket inte går om det är mellanslag i det.
</div>

## Förtydligande av attribut

### **id**
Reguljärt uttryck: **`/^[a-zA-Z_:1-9]*$/`**

Observera att “id” inte är namnet på grillplatsen utan dess identifierare. I fältet “name” skriver du in precis hur du vill att grillplatsen ska representeras i text. “id” är enbart till för att skapa en stabil identifiering som används när man ska låta en dator hantera informationen. Du kan jämföra attributen “id” och “name” som personnummer och namn för en människa. "id" är personnummer och identifierar exakt en person. "name" är namn på personen och flera människor kan heta Ellen Ripley, men alla har ett unikt personnummer. Det är viktigt att detta attribut är stabilt över tid, att det aldrig ändras. Om en grillplats flyttas, stängs eller byter namn är det rimligt att skapa ett nytt id men tillse att det gamla IDt slutar att existera och aldrig återanvänds.

Om du redan har en unik och stabil identifierare för din grillplats i ett eget system, t.ex. ett GIS-system, ett anläggningsregister, skötselplan eller motsvarande, kan du använda denna istället som ID. Du ansvarar för att namnet är unikt och stabilt över tid. Ett ID från ett verksamhetssystem inom kommunen duger gott, om du tillser att det är just unikt. Tre exempel på ID:n från verksamhetssystem inom en fiktiv kommun:

* urn:ngsi-ld:Grillplats:se:gullspang:anlaggning:102 
* trollhattan.se-anlaggningsid-GH130
* mariestad-grillplats-T654gasthamnenparkgrillplats

Om du inte har en unik och stabil identifierare för din grillplats redan, rekommenderas du att skapa en genom att ange en kommunkod från SCB för den kommun där grillplatsen finns, följt av ett bindestreck “-” och namnet på grillplatsen utan mellanslag och utan svenska tecken. Använd A-O och a-o, inga apostrofer, accenter, skiljetecken. Kommunkod anges alltid med fyra siffror med inledande nolla om sådan finns: <br>
`[kommunkod]-[NamnUtanSvenskaTeckenEllerMellanslag]`

Uttryckt som ett reguljärt uttryck blir detta: **`/^\d{4}-[a-zA-Z]+$/`**

Exempel “0854-grillplats” 

För grillplatser utanför Sverige som saknar kommunkod, anger du 9999. Exempel för ett grillplats i Norge: 9999-Bergengrillplass

Unik och stabil identifierare för grillplatsen. Består förslagsvis av kommunkoden (4 siffror), ett bindestreck “-” och sedan namnet på grillplatsen utan Å, Ä, Ö, bindestreck, mellanslag eller andra tecken som apostrof et.c. Endast bokstäver a-z och A-Z är tillåtet.

Exempel på icke tillåtna namn:

* 0854-Gäst Hamnen
* 0854-Gästhamnen
* 0854-Gästhamnens Grillplats
* grillplats 002 Gullspång

Ovanstående namn korrigerade att bli giltiga:

* 0854-GastHamnen
* 0854-GastHamnen
* 0854-GasthamnensGrillplats
* grillplats002Gullspang

Överväg att ha en global och stabil identifierare via [wikidata](#wikidata) som kompletterar fältet [id](#id). Läs mer vid förklaringen om [Wikidata](#wikidata).

### **latitud**
WGS84 är den standard som det amerikanska systemet GPS använder för att beskriva en latitud på jordklotets yta. GPS används av många kartapplikationer. Latitud anges med ett heltal följt av en decimalpunkt “.” och 1 till 8 decimaler. Exempelvis “61.21657”. En angivelse av latitud som befinner sig på jordens södra hemisfär anges med negativt tal. Exempelvis “-53.78589”. Om koordinatens inledande heltal är noll, skall alltid nollan anges.

### **longitud**
Longitud anges med ett heltal följt av en decimalpunkt följt av 1 till 8 decimaler. Exempelvis “88.40901”. En longitud som ligger väster om WGS84-systemets meridian, anges med negativt tal, Exempelvis: “-0.158101”. Om koordinatens inledande heltal är noll, skall nollan alltid anges.

### **email**
E-postadress ska anges enligt formatspecifikation i [RFC5322](https://datatracker.ietf.org/doc/html/rfc5322) i sektionen “addr-spec”. Verifiera att du följer denna standard.  

### **visit_url**
Ange lämpligen den ingångsidan för grillplatser på kommunens eller turist-/ destinationsbolagets webbsida. Det här är den ingångssida som du önskar att besökare skall komma till för att lära sig mer om grillplatser i hela kommunen. Exempel på en sådan sida i Göteborgs Stad - [https://goteborg.se/wps/portal/start/kultur-och-fritid/fritid-och-natur/friluftsliv-natur-och/grillplatser](https://goteborg.se/wps/portal/start/kultur-och-fritid/fritid-och-natur/friluftsliv-natur-och/grillplatser).

Du rekommenderas starkt att använda detta attribut för att leda besökare till mer information kring grillplats i kommunen. 

### **restrictions**
Detta attribut beskriver exempelvis om rörelsehindrade har företräde, hundar måste hållas kopplade, tillgänglighetshinder i form av vägbom eller att området är stängt vissa tider, tillgång till parkeringsplats, parkeringsavgift etc. (Möjlighet till parkering beskrivs i attribut 31). Här kan du beskriva om det behövs fiskekort eller om regler för att göra upp eld, fågelskydd eller annat förekommer på platsen vissa perioder. 

### **drinking_water**
Attributet beskriver förekomst av, inte på vilket sätt (slang, kran, pump, hink, källa osv). Ange om vatten stängs av under vinterhalvår eller motsvarande. under egenskapen “restrictions” (attribut nummer 14).  

### **wheelchair**
Attributet beskriver till vilken grad en grillplats är tillgänglighetsanpassad för rullstolar. Här anges "yes" i det fallet att man kan utnyttja hela grillplatsen med rullstol, d.v.s. att det inte finns hinder i form av trappor eller kanter för att ta sig in på och nyttja hela grillplatsen. 

"limited" anges i det fall vissa delar av grillplatsen kan utnyttjas men inte hela området. "no" anges om man inte kan alls med rullstol kan komma åt grillplatsens ytor. Ta reda på status eller lämna tomt vid första publicering om data inte finns vid tillfället.

### **service_interval**
Anges enligt [frekvens](https://docs.dataportal.se/dcat/sv/#5.4). Om man inte vet när och OM grillplatsen underhålls anges [UNKNOWN](http://publications.europa.eu/resource/authority/frequency/UNKNOWN)

### **updated**
Detta attribut ska anges när någon del av informationen har uppdaterats, hur liten och till synes obetydlig uppdateringen är. Utan datum blir det svårt för en implementatör att veta hur aktuell datat är och ifall det går att lita på. Datum ska anges på format enligt [ISO 8601](https://www.iso.org/iso-8601-date-and-time-format.html) utan undantag. 

Exempel, onsdagen den 21:e juli 2021 ska anges enligt:
2021-07-21

### **description**
Du behöver inte skriva att grillplatsen har sittplatser då det framgår genom att sätta true i attribut nr 25 “seating”. 

Exempel på beskrivning: “Grillplats beläget nära naturområde med möjlighet till utflykt.”

Vill du dela data på flera olika språk? Se [språkstöd](#sprakstod) och [beskrivning i datakatalog](#beskrivning-i-datakatalog) för att lära dig mer om kraven på metadata i samband med publicering. 

### **td_url**
Det finns inga unika ID:n att länka till utan du länkar till hela den webbadress (URL) som går direkt till ett objekt som är upplagt i Tillgänglighetsdatabasen. 
