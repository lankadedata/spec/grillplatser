# Länkar

## Grillplatser.nu

Sajten [grillplatser.nu](https://grillplatser.nu/) och dess skapare/ägare Christer Olsson har varit till stor del behjälplig att utforma och komma med idéer till denna specifikation.

Om din verksamhet publicerar data enligt formatet i specifikationen, kan hemsidan [grillplatser.nu](https://grillplatser.nu/) läsa in och automatisk publicera dina grillplatser där, utan att du behöver lägga upp dem manuellt. 

Om du följer rekommendationerna i detta dokument, [Beskrivning i datakatalog](#beskrivning-i-datakatalog) kommer hemsidan [grillplatser.nu](https://grillplatser.nu/) att kunna hitta din data. Det viktiga är att du pekar ut den här specifikationen som [uppfyller](https://docs.dataportal.se/dcat/sv/#dcat_Dataset-dcterms_conformsTo), då kan alla som skapar implementationer läsa in data i denna specifikations format. 


## Wikidata

På initiativet [Wikidata](https://www.wikidata.org) görs ett gediget arbete för att kunna länka och bygga kunskap utefter data. Genom att göra den här specifikationen kompatibel med Wikidata genom länkning hoppas vi kunna bidra till att höja kvaliteten i Wikidata och i specifikationen. Vill du att dina grillplatser i kommunen syns bättre i dessa plattformar - se till att bilder på grillplatsen finns tillgängliga med med fri licens som [CC0](https://creativecommons.org/publicdomain/zero/1.0/deed.sv) eller ladda upp bilder på [WikiCommons](https://commons.wikimedia.org/wiki/Main_Page).

Använd fältet “wikidata” i specifikationens [datamodell](#datamodell) för att länka till det objekt i Wikidata som beskriver grillplatsen. Det kan komma att finnas ett unikt ID i Wikidata för varje grillplats.

ID i Wikidata anges med en så kallad “Q-kod” och börjar med bokstaven Q och följs sedan av ett antal siffror. Detta är det unika ID som grillplatsen har i Wikidatas databas. Om det inte finns en Q-kod för din grillplats, rekommenderas du att lägga till grillplatsen på Wikidata och länka dit.

Om grillplatsen finns med i Wikidata öppnas fler möjligheter t.ex. att den enklare kan visas i sökresultat från sökmotorer eller i kartapplikationer. En rekommendationen är därför att du skapar ett objekt på Wikidata som beskriver din grillplats. 

## Tillgänglighetsdatabasen

Tillgänglighetsdatabasen (TD) saknar persistenta identifierare som är stabila över tid och det går inte att hänvisa till ett objekt på ett stabilt sätt annat än med hjälp av URL direkt till objektet som beskrivs. Tips har lämnats till TD om att en sådan identifierare kanske kunde skapas och göras synlig/tydlig i API och på webbplatsen, så att länkning av data kan ske på ett bra sätt. 

TD har ett API som du kan nå på
[Tillgänglighetsdatabasen - developer portal (azure-api.net)](https://td.portal.azure-api.net/) och som är kostnadsfritt. För att använda API:erna måste du dock registrera dig och hämta ut en personlig nyckel. APIt och dess utveckling sköts helt av Tillgänglighetsdatabasen och frågor om dess kapabilitet och funktion hänvisar vi till TD. Lämna gärna förbättringsförslag till TD som har indikerat till oss att de är intresserade av att deras tjänster nyttjas och utvecklas.

### Min grillplats saknas i Tillgänglighetsdatabasen

Kontakta din kommun där grillplatsen ligger och be dem lägga till grillplatsen. Generellt kan en del grillplatser som inte finns i kommunens ägor saknas. Se kommunens kontaktuppgifter till datamängden grillplatser.
